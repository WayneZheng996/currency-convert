import { BrowserRouter} from "react-router-dom";
import Home from "./components/Home";
import Nav from "./components/Nav";
import ExchangeRateBoard from "./components/ExchangeRateBoard";
import FooterComponent from "./components/FooterComponent"

function App() {
  return (
    <>
      <BrowserRouter>
        <Nav />
        <Home />
        <ExchangeRateBoard/>
        <FooterComponent />
      </BrowserRouter>
    </>
  );
}

export default App;
