import React, { useState, useEffect } from "react";
import { AiOutlineStock } from "react-icons/ai";
import { RiMoneyCnyBoxFill } from "react-icons/ri";
import { TbMoneybag } from "react-icons/tb";
import { AiOutlineEuroCircle } from "react-icons/ai";
import "./ExchangeRateBoard.css";

function ExchangeRateBoard() {  // board that shows conversion from one currency to another
	const [cnyRate, setCnyRate] = useState("")
	const [eurRate, setEurRate] = useState("")
	const exchangeDollarIncrement = [1,5,10,25,50,100,500,1000,5000,10000,50000]
	const toDecimalMark = number => number.toLocaleString('en-US')  // convert number into decimal and comma format

	useEffect(()=> {  // initialize and fetch exchange rate from backend
		async function getExchangeRateData(){
            const response = await fetch('http://localhost:8080/api/exchange-rates/')
            if (response.ok){
                const ExchangeRateData = await response.json();
				setCnyRate(ExchangeRateData['rates']['CNY'])
				setEurRate(ExchangeRateData['rates']['EUR'])
            }
        }
        getExchangeRateData()
	})
  return (
    <>
      <div id="exchangeboard-header">
		<h1><AiOutlineStock className="trade-icon"/>Exchange Rate Board</h1>
		<h3>Based in USD</h3>
	</div>
	<div class="container text-center" id="exchangeBoard">
		<div class="row row-cols-1 row-cols-md-2 g-4" id="card-group">
			<div class="col">
				<div class="card">
					<div class="card-header">
						<h5><b>Convert US Dollar to Chinese Yuan</b></h5>
					</div>
					<table>
						<thead>
							<tr>
								<td><b><TbMoneybag id="usd-icon"/>  USD</b></td>
								<td><b><RiMoneyCnyBoxFill id="cny-icon"/>  CNY</b></td>
							</tr>
						</thead>
						<tbody>
							{exchangeDollarIncrement.map((i) => {
								return (
								<tr>
									<td>{toDecimalMark(i)} USD</td>
									<td>{toDecimalMark(parseFloat(i*cnyRate).toFixed(2))} CNY</td>
								</tr>
								)
							})}
						</tbody>
					</table>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<div class="card-header">
						<h5><b>Convert US Dollar to Euro</b></h5>
					</div>
					<table>
						<thead>
							<tr>
								<td><b><TbMoneybag id="usd-icon"/>  USD</b></td>
								<td><b><AiOutlineEuroCircle id="eur-icon"/>  EUR</b></td>
							</tr>
						</thead>
						<tbody>
							{exchangeDollarIncrement.map((i) => {
								return (
								<tr>
									<td>{toDecimalMark(i)} USD</td>
									<td>{toDecimalMark(parseFloat(i*eurRate).toFixed(2))} EUR</td>
								</tr>
								)
							})}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
    </>
  );
}

export default ExchangeRateBoard;
