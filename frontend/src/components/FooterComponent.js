import React from 'react'
import './FooterComponent.css'
import { AiOutlineLinkedin } from "react-icons/ai";
import { AiFillGitlab } from "react-icons/ai";
import { AiTwotoneMail } from "react-icons/ai";
import { ImNewspaper } from "react-icons/im";

function FooterComponent() {
  return (
    <footer>
      <div className='footer-container'>
        <a href="https://www.linkedin.com/in/weizheng996/"><AiOutlineLinkedin className='social-icon'/></a>
        <a href="https://gitlab.com/WayneZheng996/currency-convert/-/tree/main"><AiFillGitlab className='social-icon'/></a>
        <a href="mailto:waynezheng95@gmail.com"><AiTwotoneMail className='social-icon'/></a>
        <a href="https://drive.google.com/file/d/1Wva8LmsHl3OfqqT0TCSsp39xyEPmIaTs/view?usp=share_link"><ImNewspaper className='social-icon'/></a>
        <div className='greeting'>
          <p>Thank you for browsing</p>
        </div>
      </div>
    </footer>
  )
}

export default FooterComponent
