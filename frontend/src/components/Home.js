import React, { useState, useEffect } from "react";
import "./Home.css";
import {GiWorld} from 'react-icons/gi'
import {MdOutlineSwapHoriz} from 'react-icons/md'

function Home() {
    const [countrySymbols, setCountrySymbols] = useState({})
    const [fromCurrency, setFromCurrency] = useState('')
    const [toCurrency, setToCurrency] = useState('')
    const [fromCurrencyPlaceHolder, setFromCurrencyPlaceHolder] = useState('Select a Country Currency')
    const [toCurrencyPlaceHolder, setToCurrencyPlaceHolder] = useState('Select a Country Currency')
    const [amount, setAmount] = useState('')
    const [resultAmount, setResultAmount] = useState('')
    const [exchangedRate, setExchangedRate] = useState('')
    const [displayResult, setDisplayResult] = useState('none')
    const [swapped, setSwapped] = useState(false)
    const toDecimalMark = number => number.toLocaleString('en-US')  // convert number into decimal and comma format

    async function convertCurrency() {
        if (amount>1 && fromCurrency !== '' && toCurrency!==''){
            const data = {
                "from_currency": fromCurrency,
                "to_currency": toCurrency,
                "balance": parseFloat(amount)
            }
            console.log(data)
            const response = await fetch("http://localhost:8080/api/convert/", {
                method: "POST",
                body: JSON.stringify(data)

            })
            if (response.ok) {
                const exchangeResult = await response.json();
                setResultAmount(exchangeResult['result'])
                setExchangedRate(exchangeResult['exchanged_rate'])
                setDisplayResult('block')
            }
        }
    }

    function swapCurrencyElement(event){
        // swapping the place holder element to display the corresponding new element data
        const swapCurrencyHolder = toCurrencyPlaceHolder
        setToCurrencyPlaceHolder(fromCurrencyPlaceHolder)
        setFromCurrencyPlaceHolder(swapCurrencyHolder)

        // swapping the actual conversion element to calculate the new conversion
        const swapCurrency = toCurrency
        setToCurrency(fromCurrency)
        setFromCurrency(swapCurrency)

        // Hide the result
        setDisplayResult('none')

        setSwapped(true)

    }
    // setting timeout for auto convert, after user stop typing for x amount of time
    useEffect(() => {
        const delay = setTimeout(() => {
            if (amount > 1){
                convertCurrency()
            }
        }, 1000)

        return () => clearTimeout(delay), setDisplayResult('none')
      }, [amount])


    // auto converts after two currency position being swapped
    useEffect(() => {
        const delay = setTimeout(() => {
            if (swapped && fromCurrency !== '' && toCurrency!==''){
                convertCurrency()
                setSwapped(false)
            }
        }, 500)

        return () => clearTimeout(delay)
      }, [swapped])

    // initialize data for currencies
    useEffect(() => {
        async function getCountriesData(){
            const response = await fetch('http://localhost:8080/api/symbols/')
            if (response.ok){
                const countriesData = await response.json();
                setCountrySymbols(countriesData['countries'])
            }
        }
        getCountriesData()
    }, [])

    return (
        <>
        <div className="logoDiv" id="converter-header">
            <h1><GiWorld className="world-icon"/>  Currency Converter</h1>
        </div>
        <div className="Converter container-lg">
            <div className="wrapper-row">
                <div className="wrapper-inside">
                    <div><b>Amount: </b></div>
                    <div className="input-group">
                        <span className="input-group-text" id="basic-addon1">{fromCurrency}</span>
                        <input onChange={(e) => setAmount(e.target.value)} value={parseFloat(amount)? parseFloat(amount): ''} type="text" className="form-control" placeholder="Amount" aria-label="Amount" aria-describedby="basic-addon1"/>
                    </div>
                </div>
                <div className="wrapper-inside">
                    <div><b>From: </b></div>
                    <div className="dropdown">
                        <button className="btn btn-secondary dropdown-toggle dropdown-button" type="button" id="from-currency" data-bs-toggle="dropdown" aria-expanded="false">
                            {fromCurrencyPlaceHolder}
                        </button>
                        <ul className="dropdown-menu">
                            {Object.entries(countrySymbols).map(([key, value]) => {
                                    return (
                                        <li className="dropdown-item" onClick={() => [setFromCurrency(key), setFromCurrencyPlaceHolder(`${key} - ${value['description']}`), setDisplayResult('none')]} key={key} value={key}>{`${key} - ${value['description']}`}</li>
                                    )
                                })}
                        </ul>
                    </div>
                </div>
                <div className="wrapper-inside icon-wrapper">
                    <div className="wrapper-inside icon-wrapper">
                        <div><p></p></div>
                        <MdOutlineSwapHoriz className="swap-icon" onClick={swapCurrencyElement}/>
                    </div>
                </div>
                <div className="wrapper-inside">
                    <div><b>To: </b></div>
                    <div className="dropdown">
                        <button className="btn btn-secondary dropdown-toggle dropdown-button" type="button" id="to-currency" data-bs-toggle="dropdown" aria-expanded="false">
                            {toCurrencyPlaceHolder}
                        </button>
                        <ul className="dropdown-menu">
                            {Object.entries(countrySymbols).map(([key, value]) => {
                                    return (
                                        <li className="dropdown-item" onClick={() => [setToCurrency(key), setToCurrencyPlaceHolder(`${key} - ${value['description']}`), setDisplayResult('none')]} key={key} value={key}>{`${key} - ${value['description']}`}</li>
                                    )
                                })}
                        </ul>
                    </div>
                </div>
            </div>
            <div className="result-block" style={{display: displayResult}}>
                <p id="result">{`${toDecimalMark(parseFloat(amount))} ${fromCurrency} = ${resultAmount} ${toCurrency}`}</p>
                <p id="exchange-rate">{`1 ${fromCurrency} = ${exchangedRate} ${toCurrency}`}</p>
            </div>
            <button type="button" className="btn btn-primary float-end mt-3" onClick={convertCurrency}>Convert</button>
        </div>
        </>
    )
    }

export default Home;
