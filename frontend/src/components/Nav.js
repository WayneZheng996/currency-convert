import React from "react";
import "./Nav.css";


function Nav() {
  return (
    <>
    <nav>
      <a href="/">CONVERTER</a>
      <a href="https://gitlab.com/WayneZheng996/currency-convert/-/tree/main">ABOUT</a>
      <div id="indicator"></div>
    </nav>
    </>
  );
}

export default Nav;
