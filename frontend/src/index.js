import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import App from './App';
import "./App.css"


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
