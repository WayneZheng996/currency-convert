from django.urls import path
from .api_views import (
    get_coutry_symbols,
    convert_currency,
    exchange_rate_table
)


urlpatterns = [
    path('symbols/', get_coutry_symbols, name= "get_coutry_symbols"),
    path('convert/', convert_currency, name='convert_currency'),
    path('exchange-rates/', exchange_rate_table, name='exchange_rates'),
]
