from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import requests
import json


@require_http_methods(["GET"])
def get_coutry_symbols(request):  # fetch for all available currency convertable countries
    if request.method == "GET":
        try:
            response = requests.get("https://api.exchangerate.host/symbols")
            if response.status_code == 200:
                countries_hashtable = response.json()['symbols']
                return JsonResponse({"countries": countries_hashtable}, status=200)
        except:
            return JsonResponse({"Message: Service unavailable"}, status=400)


@require_http_methods(["POST"])
def convert_currency(request):  # required params in key:value body [from_currency -> country symbols, to_currency -> country symbols, balance -> float]
    if request.method == "POST":
        content = json.loads(request.body)
        try:  # validating request body and 3rd party request
            from_currency = content['from_currency']
            to_currency = content['to_currency']
            balance = content['balance']
            response = requests.get(f"https://api.exchangerate.host/convert?from={from_currency}&to={to_currency}")
            if response.status_code == 200:
                conversion_rate = response.json()['result']
                converted_result =  f'{round(balance * conversion_rate, 2):,}'  # get the result, and convert, rounded it separated with commas
                return JsonResponse({"result": converted_result, "exchanged_rate": conversion_rate}, status=200)
        except KeyError:  # if the content body does not have required params
            return JsonResponse({"Message": "Key does not match"}, status=400)
        except:
            return JsonResponse({"Message": "Service unavailable"}, status=400)



@require_http_methods(["GET"])
def exchange_rate_table(request):  # fetch exchange rate for varies countries to be displayed on frontend exchange table
    if request.method == "GET":
        try:
            desired_rates = ["EUR", "JPY", "CNY", "CAD"]
            response = requests.get(f'https://api.exchangerate.host/latest?base=USD&symbols={",".join(desired_rates)}', timeout=10)
            if response.status_code == 200:
                exchange_rate_hashtable = response.json()['rates']
                return JsonResponse({
                    "base_currency": "USD",
                    "rates": exchange_rate_hashtable
                }, status=200)
        except TimeoutError:
            return JsonResponse({"Message": "Server timeout"}, status=504)
        except:
            return JsonResponse({"Message": "Unknown Server Error"}, status=500)
