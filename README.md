
![Design](images/currency-covert.png)


# Currency Converter


### Requirements:

- Description:
Create a simple currency converter where users can enter an amount in one currency and convert it to another currency. The app should have two drop-down menus where users can select the currencies they want to convert from and to. When the user enters an amount and selects the currencies, the app should display the converted amount. The backend should retrieve currency exchange rates from a currency API.
Requirements for Currency Converter:




- Frontend:
    The user should be able to enter an amount in one currency and convert it to another currency
    The app should have two drop-down menus where users can select the currencies they want to convert from and to
    When the user selects the currencies and enters an amount, the app should display the converted amount

- Backend:
    The backend should be able to handle API endpoints and retrieve currency exchange rates from a currency API
    The backend should convert the amount to the target currency using the retrieved exchange rate and return the converted amount to the frontend
    The backend should handle errors and return appropriate error messages to the frontend

- General:
    The code should be well-structured, easy to maintain, and readable with comments.






## Journal:

### 03/08/2023

- initialize project design and runtime environment
- set up base structure for project
- finished backend api routing to 3rd party service (fetching available countries, convert currency ratio)


### 03/09/2023
- Completed Frontend logic and rendering
- Completed frontend functionality
- added not number detection, avoid people type in non numbers
- added mobile display compatibility
- added SWAP currency button
- added auto convert when increment/decrement amount


### 03/10/2023
- Added Footer Icon to navigate linkedin, Gitlab, Email, and Resume
- Fixed all component phone compatibility issue
- Added exchange Currency table from USD -> CNY , USD -> EUR
